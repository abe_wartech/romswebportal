import React, { Component } from "react";
import { Form, BackTop, Row, Col, Typography, Button } from "antd";
import Navbar from "./Components/Navbar";
import YouTube from "react-youtube";
import AOS from "aos";
import "aos/dist/aos.css";
import "antd/dist/antd.less";
import "./App.scss";

const { Title } = Typography;
const kartun = require("./assets/kartun_true_love_cy8x.svg");
const x1 = require("./assets/x1_new_ideas_jdea.svg");
const x2 = require("./assets/x2_progress_data_4ebj.svg");
const x3 = require("./assets/x3_celebration_0jvk.svg");
const x5 = require("./assets/x5_share_link_qtxe.svg");
const x6 = require("./assets/x6_fingerprint_swrc.svg");
const x7 = require("./assets/x7_Login_v483.svg");
const x4a = require("./assets/x4a.svg");
const x4b = require("./assets/x4b.svg");
const x4c = require("./assets/x4c.svg");
const x4d = require("./assets/x4d.svg");
const x4e = require("./assets/x4e.svg");
const facebook = require("./assets/facebook.svg");
const instagram = require("./assets/instagram.svg");
const twitter = require("./assets/twitter.svg");
const LOGO = require("./assets/logom.png");
const ip6 = require("./assets/goodteam.svg");
const news = require("./assets/sbnews2.png");

class App extends Component {
  state = {
    width: window.innerWidth
  };

  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50
    });
  };

  UNSAFE_componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const { width } = this.state;
    const isMobile = width <= 500;
    const opts = {
      height: "390",
      width: "640",
      playerVars: {
        autoplay: 0,
        controls: 0,
        fs: 1,
        loop: 1,
        modestbranding: 0
      }
    };
    //untuk pc//
    if (!isMobile) {
      return (
        <div>
          <Navbar></Navbar>
          <Row className="section1" type="flex" justify="center">
            <Col xs={24} md={12} style={{ marginTop: 100 }}>
              <Title
                data-aos="zoom-in-up"
                level={1}
                style={{ fontSize: "3.5vw" }}
              >
                Belajar Investasi Bersama MANTUL TRADER
              </Title>
              <Typography
                data-aos="zoom-in-left"
                data-aos-delay="550"
                style={{ fontSize: "1.5vw" }}
              >
                Mantul Trader adalah aplikasi untuk kamu yang ingin belajar,
                berdiskusi dan analisa manrket dalam satu tempat.
              </Typography>
              <Row
                data-aos="slide-right"
                data-aos-delay="850"
                style={{ marginTop: 60 }}
              >
                <Button type="primary" size="large">
                  MULAI BELAJAR INVESTASI
                </Button>
              </Row>
              {/* <Row style={{ marginTop: 60 }}>
                <img
                  // src="https://stockbit.com/assets/template/4b70f6fae447.png"
                  style={{ height: 34, marginRight: 30 }}
                ></img>
                <img
                  // src="https://stockbit.com/assets/template/f06b908907d5.png"
                  style={{ height: 34 }}
                ></img>
              </Row> */}
            </Col>
            <Col xs={24} md={12}>
              <Row>
                <img
                  src={kartun}
                  style={{ width: "97%", marginTop: 170 }}
                ></img>
                {/* <img
                  src="https://stockbit.com/assets/template/iphone6.png"
                  style={{ width: 340, marginTop: -20 }}
                ></img> */}
              </Row>
            </Col>
          </Row>
          <Row className="section2" justify="center">
            <Col
              data-aos="slide-right"
              data-aos-delay="550"
              xs={24}
              align="center"
              style={{ marginBottom: 30 }}
            >
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Keuntungan dari Kebersamaan
              </Title>
            </Col>
            <Row
              type="flex"
              gutter={48}
              justify="space-around"
              style={{ padding: 15 }}
              data-aos="slide-right"
            >
              <Col md={8} align="center">
                <img src={x1} style={{ width: "40%" }}></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "2vw" }}
                >
                  Simple Method
                </Title>
                <Typography style={{ fontSize: "1.25vw" }}>
                  Dapatkan method yang simple dan mudah dimengerti baik untuk
                  pemula dan berpengalaman.
                </Typography>
              </Col>
              <Col md={8} align="center">
                <img src={x2} style={{ width: "40%" }}></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "2vw" }}
                >
                  Great Return & Limited Risk
                </Title>
                <Typography style={{ fontSize: "1.25vw" }}>
                  Dapatkan hasil yang baik dengan resiko yang kecil saat
                  melakukan eksekusi pada market.
                </Typography>
              </Col>
              <Col md={8} align="center">
                <img src={x3} style={{ width: "40%" }}></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "2vw" }}
                >
                  Lebih Seru
                </Title>
                <Typography style={{ fontSize: "1.25vw" }}>
                  Investasi jadi lebih seru bareng teman dan keluarga kamu.
                  Bagikan strategi kamu untuk saling melengkapi dan nikmati
                  hasilnya bersama.
                </Typography>
              </Col>
            </Row>
          </Row>
          <Row className="section3" justify="center">
            <Col md={10} style={{ paddingLeft: 50 }}>
              <img src={ip6} style={{ width: 340, marginTop: 150 }}></img>
            </Col>
            <Col md={14} style={{ paddingRight: 100, marginTop: 80 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Mudahnya Metode Mantul Trader
              </Title>
              <Row style={{ marginBottom: 30, marginTop: 35 }}>
                <img data-aos="zoom-in-up" data-aos-delay="250" src={x4a}></img>
                <img src={x4b}></img>
                <img data-aos="zoom-in-up" data-aos-delay="650" src={x4c}></img>
                <img src={x4d}></img>
                <img
                  data-aos="zoom-in-up"
                  data-aos-delay="1050"
                  src={x4e}
                ></img>
              </Row>
              <Typography style={{ fontSize: "1.25vw" }}>
                Didesain untuk investor millenial, kamu dapat invest di market
                ternama dengan mudah untuk meraih mimpimu menuju kebebasan
                finansial.
              </Typography>
            </Col>
          </Row>
          <Row className="section4" justify="center">
            <Col md={24} align="center">
              INTRODUCTION VIDEO
            </Col>
            <Row style={{ marginTop: 100 }} justify="center">
              <Col
                md={12}
                align="center"
                style={{ marginLeft: 300, marginBottom: 100 }}
              >
                <YouTube
                  data-aos="zoom-in-down"
                  opts={opts}
                  videoId="VJW-m28Kpko"
                ></YouTube>
              </Col>
            </Row>
          </Row>
          <Row className="section5" justify="center">
            <Col xs={24} align="center" style={{ marginBottom: 30 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Professional Tools bahkan untuk Pemula
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Investasi bukan hanya untuk kalangan atas. Mantul Trader
                dilengkapi dengan data finansial dan fitur kelas professional
                untuk mendukung investasi semua orang.
              </Typography>
            </Col>
          </Row>
          <Row className="section6" justify="center">
            <Col md={16}>
              <img src={news} style={{ width: 800 }}></img>
            </Col>
            <Col md={6} style={{ marginTop: 60 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Buying Low Selling High
              </Title>
              <Typography data-aos="zoom-in-up" style={{ fontSize: "1.25vw" }}>
                Ayo dapatkan profit di marketmu sekarang juga!
              </Typography>
            </Col>
          </Row>
          <Row className="section7" justify="space-around">
            <Col
              data-aos="slide-right"
              data-aos-delay="550"
              md={12}
              align="center"
            >
              <img src={x5} style={{ width: "50%", marginBottom: 30 }}></img>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Belajar Bersama Dengan Mantul Trader
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Kamu dapat berlatih dan menguji strategi investasi tanpa resiko
                dengan metode di Mantul Trader.
              </Typography>
            </Col>
            <Col
              data-aos="zoom-in-up"
              data-aos-delay="550"
              md={12}
              align="center"
            >
              <img src={x6} style={{ width: "40%", marginBottom: 20 }}></img>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Aman & Terpercaya
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Hindari belajar investasi bodong. Kamu dapat berlatih dan
                menguji strategi investasi tanpa resiko dengan metode di Mantul
                Trader.
              </Typography>
            </Col>
          </Row>
          <Row className="section8" justify="center">
            <Col xs={24} align="center" style={{ marginBottom: 30 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Yuk Gabung!
              </Title>
              <Typography data-aos="zoom-in-up" style={{ fontSize: "1.25vw" }}>
                Kamu dapat berlatih dan menguji strategi investasi tanpa resiko.
              </Typography>
              <img src={x7} style={{ width: "30%", marginTop: 50 }}></img>
              <Typography
                data-aos="zoom-in-up"
                data-aos-delay="250"
                style={{ fontSize: "1.25vw", marginTop: 80 }}
              >
                Daftar sekarang dan bergabunglah dengan team Mantul Trader.
              </Typography>
              <Button
                data-aos="zoom-in-right"
                data-aos-delay="350"
                type="primary"
                size="large"
                style={{ marginTop: 50 }}
              >
                <a href="/more">Mulai Belajar Take Profit</a>
              </Button>
              <Typography
                data-aos="zoom-in-right"
                data-aos-delay="650"
                style={{ fontSize: "1.25vw", marginTop: 50 }}
              >
                Dengan mengklik tombol di atas, Anda berarti telah menyetujui
                Kebijakan Privasi & Persyaratan Layanan Mantul Trader.
              </Typography>
            </Col>
          </Row>
          <Row className="section9" justify="center">
            <Col md={4}>
              <img src={LOGO} style={{ width: "35%" }}></img>
              <Typography style={{ marginTop: 10 }}>
                &copy; Mantul Trader 2020
              </Typography>
              <Typography></Typography>
            </Col>
            <Col md={4}>
              <Row>
                <img
                  src={facebook}
                  style={{
                    height: 20,
                    filter: "grayscale(100%)",
                    marginRight: 15
                  }}
                ></img>
                <img
                  src={twitter}
                  style={{
                    height: 20,
                    filter: "grayscale(100%)",
                    marginRight: 15
                  }}
                ></img>
                <img
                  src={instagram}
                  style={{ height: 20, filter: "grayscale(100%)" }}
                ></img>
              </Row>
            </Col>
            <Col md={4}>
              <Typography>
                <a href="/aboutus">About Us</a>
              </Typography>
            </Col>
            <Col md={4}>
              {/* <Typography><a href="">House rules</a></Typography> */}
              <Typography>
                <a href="/contact">Contact</a>
              </Typography>
            </Col>
            <Col md={4}>
              <Typography>
                <a href="/risk">Terms & Privacy </a>
              </Typography>
              <img
                // src="https://stockbit.com/assets/template/4b70f6fae447.png"
                style={{ height: 34, marginTop: 10 }}
              ></img>
              <img
                // src="https://stockbit.com/assets/template/f06b908907d5.png"
                style={{ height: 34, marginTop: 10 }}
              ></img>
            </Col>
            <Col md={4}>
              <Typography></Typography>
              <img
                // src="https://stockbit.com/assets/template/logo-fintech.png"
                style={{ height: 40, marginTop: 10 }}
              ></img>
            </Col>
          </Row>
          <BackTop visibilityHeight={450} />
        </div>
      );
    } else {
      //untuk mobile//
      return (
        <div>
          <Navbar></Navbar>
          <Row className="section1-mobile" type="flex" justify="center">
            <Col xs={24} md={12} style={{ marginTop: 50 }} align="center">
              <Title
                data-aos="zoom-in-up"
                level={1}
                style={{ fontSize: "6vw" }}
              >
                Belajar Take Profit Bersama MANTUL TRADER
              </Title>
              <Typography
                data-aos="zoom-in-left"
                data-aos-delay="550"
                style={{ fontSize: "3vw" }}
              >
                Mantul Trader adalah aplikasi untuk kamu yang ingin
                belajar,berdiskusi dan analisa dalam satu tempat.
              </Typography>
              <Row
                data-aos="slide-right"
                data-aos-delay="850"
                style={{ marginTop: 60 }}
              >
                <Button type="primary" size="large">
                  MULAI BELAJAR DI MANTUL TRADER
                </Button>
              </Row>
              <Row style={{ marginTop: 60 }}>
                <img
                  src="https://stockbit.com/assets/template/4b70f6fae447.png"
                  style={{ height: 34, marginRight: 30 }}
                ></img>
                <img
                  src="https://stockbit.com/assets/template/f06b908907d5.png"
                  style={{ height: 34 }}
                ></img>
              </Row>
            </Col>
            <Col xs={24} md={12} style={{ marginTop: 50 }}>
              <Row>
                <img
                  src="https://stockbit.com/assets/template/iphone6.png"
                  style={{ width: 330 }}
                ></img>
              </Row>
            </Col>
          </Row>
          <Row className="section2-mobile" justify="center">
            <Col
              data-aos="slide-right"
              data-aos-delay="550"
              xs={24}
              align="center"
            >
              <Title
                level={1}
                className="text-white"
                style={{ fontSize: "6vw" }}
              >
                Keuntungan dari Kebersamaan
              </Title>
            </Col>
            <Row
              type="flex"
              justify="space-around"
              style={{ padding: 25 }}
              data-aos="slide-right"
            >
              <Col md={8} align="center">
                <img src={x1} width="80%"></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "3vw" }}
                >
                  Lebih Informatif
                </Title>
                <Typography style={{ fontSize: "2.2vw" }}>
                  Dapatkan ide investasi yang baik dari hasil diskusi dan
                  analisa market investor yang berpengalaman.
                </Typography>
              </Col>
              <Col md={8} align="center">
                <img src={x2} width="80%"></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "3vw" }}
                >
                  Lebih Cepat
                </Title>
                <Typography style={{ fontSize: "2.2vw" }}>
                  Informasi tersebar lebih cepat dengan bantuan komunitas Mantul
                  Trader. Bersama Mantul Trader, kamu akan lebih terupdate
                  dengan informasi terkini.
                </Typography>
              </Col>
              <Col md={8} align="center">
                <img src={x3} width="80%"></img>
                <Title
                  level={4}
                  className="text-white"
                  style={{ fontSize: "3vw" }}
                >
                  Lebih Seru
                </Title>
                <Typography style={{ fontSize: "2.2vw" }}>
                  Investasi jadi lebih seru bareng teman dan keluarga kamu.
                  Bagikan strategi kamu untuk saling melengkapi dan nikmati
                  hasilnya bersama.
                </Typography>
              </Col>
            </Row>
          </Row>
          <Row className="section3-mobile" justify="center">
            <Col md={10}>
              <img
                src="https://stockbit.com/assets/template/iphone6.png"
                style={{ width: 340 }}
              ></img>
            </Col>
            <Col md={14} style={{ marginTop: 50 }} align="center">
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "4vw" }}
              >
                Mudahnya Bertransaksi dalam 3 Langkah
              </Title>
              <Row style={{ marginBottom: 30, marginTop: 35 }}>
                <Col sm={8} xs={8}>
                  <img
                    data-aos="zoom-in-up"
                    data-aos-delay="250"
                    src={x4a}
                  ></img>
                </Col>
                <Col sm={8} xs={8}>
                  <img
                    data-aos="zoom-in-up"
                    data-aos-delay="650"
                    src={x4c}
                  ></img>
                </Col>
                <Col sm={8} xs={8}>
                  <img
                    data-aos="zoom-in-up"
                    data-aos-delay="1050"
                    src={x4e}
                  ></img>
                </Col>
              </Row>
              <Typography style={{ fontSize: "2vw" }}>
                Didesain untuk investor millenial, kamu dapat invest di market
                ternama dengan mudah untuk meraih mimpimu menuju kebebasan
                finansial.
              </Typography>
            </Col>
          </Row>
          <Row className="section4-mobile" justify="center">
            <Col md={24} align="center">
              INTRODUCTION VIDEO
            </Col>
            <Row style={{ marginTop: 50 }}>
              <Col md={12} align="center" style={{ marginBottom: 50 }}>
                <YouTube
                  data-aos="zoom-in-down"
                  opts={opts}
                  videoId="VJW-m28Kpko"
                ></YouTube>
              </Col>
            </Row>
          </Row>
          <Row className="section5-mobile" justify="center">
            <Col xs={24} align="center" style={{ marginBottom: 30 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Professional Tools bahkan untuk Pemula
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Investasi market bukan hanya untuk kalangan atas. Mantul Trader
                dilengkapi dengan data finansial dan fitur kelas professional
                untuk mendukung investasi semua orang.
              </Typography>
            </Col>
          </Row>
          <Row className="section6-mobile" justify="center">
            <Col md={16}>
              <img src={news} style={{ width: 800 }}></img>
            </Col>
            <Col md={6} style={{ marginTop: 60 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Berita & Laporan dari Sumber Terpercaya
              </Title>
              <Typography data-aos="zoom-in-up" style={{ fontSize: "1.25vw" }}>
                Satu tempat yang praktis untuk baca berita dari berbagai sumber.
              </Typography>
            </Col>
          </Row>
          <Row className="section7-mobile" justify="space-around">
            <Col
              data-aos="slide-right"
              data-aos-delay="550"
              md={12}
              align="center"
            >
              <img src={x5} style={{ marginBottom: 20 }}></img>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Metode Pembelajaran Investasi Digital
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Menggunakan metode analisa yang simple, resiko yang kecil, dan
                membuat investasi mengalami profit yang nyata.
              </Typography>
            </Col>
            <Col
              data-aos="zoom-in-up"
              data-aos-delay="550"
              md={12}
              align="center"
            >
              <img src={x6} style={{ marginBottom: 20 }}></img>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Simple & Profit
              </Title>
              <Typography style={{ fontSize: "1.25vw" }}>
                Hindari investasi tanpa metode yang tidak tidak efisien.
              </Typography>
            </Col>
          </Row>
          <Row className="section8-mobile" justify="center">
            <Col xs={24} align="center" style={{ marginBottom: 30 }}>
              <Title
                level={4}
                className="text-white"
                style={{ fontSize: "3vw" }}
              >
                Belajar Dulu Dengan Mantul Trader
              </Title>
              <Typography data-aos="zoom-in-up" style={{ fontSize: "1.25vw" }}>
                Kamu dapat berlatih dan menguji strategi investasi tanpa resiko
                dengan metode di Mantul Trader.
              </Typography>
              <img src={x7} style={{ marginTop: 50 }}></img>
              <Typography
                data-aos="zoom-in-up"
                data-aos-delay="250"
                style={{ fontSize: "1.25vw", marginTop: 80 }}
              >
                Daftar sekarang dan rasakan keuntungannya.
              </Typography>
              <Button
                data-aos="zoom-in-right"
                data-aos-delay="350"
                type="primary"
                size="large"
                style={{ marginTop: 50 }}
              >
                MULAI BERINVESTASI
              </Button>
              <Typography
                data-aos="zoom-in-right"
                data-aos-delay="650"
                style={{ fontSize: "1.25vw", marginTop: 50 }}
              >
                Dengan mengklik tombol di atas, Anda berarti telah menyetujui
                Kebijakan Privasi & Persyaratan Layanan Mantul Trader.
              </Typography>
            </Col>
          </Row>
          <Row className="section9-mobile" justify="center">
            <Col md={4}>
              <img src={LOGO} style={{ height: 60, width: "70%" }}></img>
              <Typography style={{ marginTop: 10 }}>
                &copy; Mantul Trader 2020
              </Typography>
              <Typography></Typography>
            </Col>
            <Col md={4}>
              <Row>
                <img
                  src={facebook}
                  style={{
                    height: 20,
                    filter: "grayscale(100%)",
                    marginRight: 15
                  }}
                ></img>
                <img
                  src={twitter}
                  style={{
                    height: 20,
                    filter: "grayscale(100%)",
                    marginRight: 15
                  }}
                ></img>
                <img
                  src={instagram}
                  style={{ height: 20, filter: "grayscale(100%)" }}
                ></img>
              </Row>
            </Col>
            <Col md={4}>
              <Typography>About</Typography>
              <Typography>Contact</Typography>
              <Typography>Help</Typography>
              <Typography>Karir</Typography>
            </Col>
            <Col md={4}>
              <Typography>House Rules</Typography>
              <Typography>Terms</Typography>
              <Typography>Privacy</Typography>
            </Col>
            <Col md={4}>
              <Typography></Typography>
              <img style={{ height: 34, marginTop: 10 }}></img>
              <img style={{ height: 34, marginTop: 10 }}></img>
            </Col>
            <Col md={4}>
              <Typography></Typography>
              <img style={{ height: 40, marginTop: 10 }}></img>
            </Col>
          </Row>
          <BackTop visibilityHeight={450} />
        </div>
      );
    }
  }
}

export default Form.create()(App);
