import React, { Component } from "react";
import { Menu, Icon } from "antd";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class RightMenu extends Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.Item key="investing">
          <a href="">Home</a>
        </Menu.Item>
        <Menu.Item key="protools">
          <a href="/method">Method</a>
        </Menu.Item>
        <Menu.Item key="blog">
          <a href="/home">Artikel</a>
        </Menu.Item>
        <Menu.Item key="help">
          <a href="/analisa">Education & Analys</a>
        </Menu.Item>
        <Menu.Item key="sign">
          <a href="/more">SIGN UP</a>
        </Menu.Item>
        <Menu.Item key="login">
          <a href="/login">LOG IN</a>
        </Menu.Item>
      </Menu>
    );
  }
}

export default RightMenu;
